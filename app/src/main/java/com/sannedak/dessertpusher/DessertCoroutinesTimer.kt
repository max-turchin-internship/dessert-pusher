package com.sannedak.dessertpusher

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.launch
import timber.log.Timber

class DessertCoroutinesTimer(
    lifecycle: Lifecycle,
    private val lifecycleScope: LifecycleCoroutineScope
) : LifecycleObserver {

    private var tickerChannel: ReceiveChannel<Unit>? = null

    var secondsCount = 0

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startTimer() {
        tickerChannel = ticker(delayMillis = 1_000, initialDelayMillis = 1_000)

        lifecycleScope.launchWhenStarted {
            for (event in requireNotNull(tickerChannel)) {
                secondsCount++
                Timber.i("Timer (Coroutines) is at: $secondsCount")
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopTimer() {
        tickerChannel?.cancel()
    }
}
