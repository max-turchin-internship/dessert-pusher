package com.sannedak.dessertpusher

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DessertRxJavaTimer(lifecycle: Lifecycle) : LifecycleObserver {

    var secondsCount = 0

    private var ticker: Disposable? = null

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startTimer() {
        ticker = Observable.interval(1, TimeUnit.SECONDS)
            .observeOn(Schedulers.single())
            .subscribe {
                secondsCount++
                Timber.i("Timer (RxJava2) is at: $secondsCount")
            }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopTimer() {
        ticker?.dispose()
    }
}
