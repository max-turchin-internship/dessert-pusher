package com.sannedak.dessertpusher.data.repository

import com.sannedak.dessertpusher.data.model.Dessert

interface DessertsRepository {

    fun getDesserts(): List<Dessert>
}
