package com.sannedak.dessertpusher.data.model

data class Dessert(
    val imageId: Int,
    val price: Int,
    val startProductionAmount: Int
)
