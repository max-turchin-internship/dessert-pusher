package com.sannedak.dessertpusher.ui

import androidx.compose.ui.graphics.Color

val purple200 = Color(0xFFBB86FC)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)

val green = Color(0xFF6AB343)
val grey = Color(0x99000000)
