package com.sannedak.dessertpusher.ui

import androidx.compose.ui.unit.dp

val defaultSpacing = 16.dp
val buttonSize = 150.dp