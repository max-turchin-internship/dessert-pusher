package com.sannedak.dessertpusher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.sannedak.dessertpusher.data.model.Dessert
import com.sannedak.dessertpusher.data.repository.DessertsRepository
import timber.log.Timber

class DessertViewModel(
    private val repository: DessertsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    companion object {
        private const val KEY_REVENUE = "revenue_key"
        private const val KEY_DESSERT_SOLD = "dessert_sold_key"
        private const val KEY_DESSERTS_LIST = "desserts_list"
    }

    private var _dessertsList: MutableLiveData<List<Dessert>> =
        savedStateHandle.getLiveData(KEY_DESSERTS_LIST, getDesserts())
    private var _revenue: MutableLiveData<Int> =
        savedStateHandle.getLiveData(KEY_REVENUE, 0)
    private var _dessertsSold: MutableLiveData<Int> =
        savedStateHandle.getLiveData(KEY_DESSERT_SOLD, 0)
    private var _currentDessert = MutableLiveData<Dessert>()

    val revenue: LiveData<Int>
        get() = _revenue

    val dessertsSold: LiveData<Int>
        get() = _dessertsSold

    val currentDessert: LiveData<Dessert>
        get() = _currentDessert

    init {
        checkCurrentDessert()
    }

    fun onDessertClicked() {
        _dessertsSold.value = _dessertsSold.value?.plus(1)
        _revenue.value = _revenue.value!! + _currentDessert.value!!.price
        checkCurrentDessert()
    }

    private fun getDesserts() = repository.getDesserts()

    private fun checkCurrentDessert() {
        for (dessert in _dessertsList.value!!) {
            if (_dessertsSold.value!! >= dessert.startProductionAmount) {
                _currentDessert.value = dessert
            } else break
        }
    }
}
