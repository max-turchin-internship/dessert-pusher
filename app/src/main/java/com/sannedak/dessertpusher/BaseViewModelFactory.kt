package com.sannedak.dessertpusher

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner

@Suppress("UNCHECKED_CAST")
class BaseViewModelFactory<T>(
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null,
    val creator: (handle: SavedStateHandle) -> T
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return creator(handle) as T
    }
}
