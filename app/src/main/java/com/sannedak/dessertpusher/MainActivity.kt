package com.sannedak.dessertpusher

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Share
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.ui.tooling.preview.Preview
import com.sannedak.dessertpusher.data.repository.DessertsMockRepository
import com.sannedak.dessertpusher.ui.*
import timber.log.Timber

private const val KEY_TIMER_SECONDS = "timer_seconds_key"
private const val KEY_TIMER_COROUTINES_SECONDS = "timer_coroutines_seconds_key"
private const val KEY_TIMER_RXJAVA2_SECONDS = "timer_rxjava2_seconds_key"

class MainActivity : AppCompatActivity() {

    private lateinit var dessertTimer: DessertTimer
    private lateinit var dessertCoroutinesTimer: DessertCoroutinesTimer
    private lateinit var dessertRxJavaTimer: DessertRxJavaTimer

    private val repository = DessertsMockRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.i("onCreate called")

        dessertTimer = DessertTimer(this.lifecycle)
        dessertCoroutinesTimer = DessertCoroutinesTimer(this.lifecycle, this.lifecycleScope)
        dessertRxJavaTimer = DessertRxJavaTimer(this.lifecycle)

        val dessertViewModel: DessertViewModel by viewModels {
            BaseViewModelFactory(this) { DessertViewModel(repository, it) }
        }

        if (savedInstanceState != null) {
            dessertTimer.secondsCount = savedInstanceState.getInt(KEY_TIMER_SECONDS, 0)
            dessertCoroutinesTimer.secondsCount =
                savedInstanceState.getInt(KEY_TIMER_COROUTINES_SECONDS, 0)
            dessertRxJavaTimer.secondsCount =
                savedInstanceState.getInt(KEY_TIMER_RXJAVA2_SECONDS, 0)
        }

        setContent {
            DessertPusherTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    DessertPusher(dessertViewModel)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_TIMER_SECONDS, dessertTimer.secondsCount)
        outState.putInt(KEY_TIMER_COROUTINES_SECONDS, dessertCoroutinesTimer.secondsCount)
        outState.putInt(KEY_TIMER_RXJAVA2_SECONDS, dessertRxJavaTimer.secondsCount)
        Timber.i("onSaveInstanceState Called")
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Timber.i("onRestoreInstanceState Called")
    }

    override fun onStart() {
        super.onStart()
        Timber.i("onStart Called")
    }

    override fun onResume() {
        super.onResume()
        Timber.i("onResume Called")
    }

    override fun onPause() {
        super.onPause()
        Timber.i("onPause Called")
    }

    override fun onStop() {
        super.onStop()
        Timber.i("onStop Called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("onDestroy Called")
    }

    override fun onRestart() {
        super.onRestart()
        Timber.i("onRestart Called")
    }
}

@Composable
private fun DessertPusher(dessertViewModel: DessertViewModel) {

    val revenue = dessertViewModel.revenue.observeAsState()
    val dessertsSold = dessertViewModel.dessertsSold.observeAsState()
    val currentDessert = dessertViewModel.currentDessert.observeAsState()

    Column(modifier = Modifier.fillMaxSize()) {
        DessertAppBar(revenue, dessertsSold)
        Box {
            Image(
                imageVector = vectorResource(id = R.drawable.bakery_back),
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
            ConstraintLayout(Modifier.fillMaxSize()) {
                val (revenueText, amountSoldText, dessertSoldText, background, dessertButton) = createRefs()

                val startBaseline = createGuidelineFromStart(defaultSpacing)
                val endBaseline = createGuidelineFromEnd(defaultSpacing)
                val bottomBaseline = createGuidelineFromBottom(defaultSpacing)

                val revenueTextModifier = Modifier.constrainAs(revenueText) {
                    end.linkTo(endBaseline)
                    bottom.linkTo(bottomBaseline)
                }
                val amountSoldTextModifier = Modifier
                    .constrainAs(amountSoldText) {
                        end.linkTo(endBaseline)
                        bottom.linkTo(revenueText.top, defaultSpacing)
                    }
                    .padding(top = defaultSpacing)
                val dessertSoldTextModifier = Modifier.constrainAs(dessertSoldText) {
                    start.linkTo(startBaseline)
                    bottom.linkTo(amountSoldText.bottom)
                }
                val dessertButtonModifier = Modifier
                    .constrainAs(dessertButton) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        bottom.linkTo(background.top)
                    }
                    .size(buttonSize)
                    .clickable(onClick = { dessertViewModel.onDessertClicked() })

                DessertButton(
                    image = vectorResource(id = currentDessert.value!!.imageId),
                    modifier = dessertButtonModifier
                )
                Surface(color = Color.White, modifier = Modifier
                    .constrainAs(background) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(amountSoldText.top)
                        bottom.linkTo(parent.bottom)
                        width = Dimension.fillToConstraints
                        height = Dimension.fillToConstraints
                    }
                ) { }
                Text(
                    text = "$${revenue.value}",
                    modifier = revenueTextModifier,
                    style = revenueStyle
                )
                Text(
                    text = "${dessertsSold.value}",
                    modifier = amountSoldTextModifier,
                    style = amountSoldStyle
                )
                Text(
                    text = stringResource(id = R.string.dessert_sold),
                    modifier = dessertSoldTextModifier,
                    style = dessertSoldStyle
                )
            }
        }
    }
}

@Composable
private fun DessertButton(image: ImageVector, modifier: Modifier) {
    Image(imageVector = image, modifier = modifier, contentScale = ContentScale.Crop)
}

@Composable
private fun DessertAppBar(revenue: State<Int?>, dessertsSold: State<Int?>) {
    val context = AmbientContext.current
    val sendIntent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(
            Intent.EXTRA_TEXT,
            context.resources.getString(R.string.share_text, dessertsSold.value, revenue.value)
        )
        type = "text/plain"
    }
    val sendItem = Intent.createChooser(sendIntent, null)

    TopAppBar(
        title = { Text(text = stringResource(id = R.string.app_name)) },
        actions = {
            IconButton(
                onClick = { startActivity(context, sendItem, null) },
                content = { Icon(imageVector = Icons.Outlined.Share) }
            )
        }
    )
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    val repository = DessertsMockRepository()
    val state = SavedStateHandle()
    val dessertViewModel = DessertViewModel(repository, state)

    DessertPusherTheme {
        DessertPusher(dessertViewModel)
    }
}
