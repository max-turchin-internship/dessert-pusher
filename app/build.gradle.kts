plugins {
    id(BuildOptions.androidApplication)
    id(BuildOptions.kotlinAndroid)
}

android {
    compileSdkVersion(AndroidSdk.compile)

    defaultConfig {
        applicationId = Config.appId
        minSdkVersion(AndroidSdk.minimal)
        targetSdkVersion(AndroidSdk.target)
        versionCode(Config.code)
        versionName(Config.name)

        testInstrumentationRunner = Config.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }

    buildFeatures {
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = Compose.Versions.composeVersion
        kotlinCompilerVersion = Compose.Versions.kotlinCompiler
    }
}

dependencies {

    // Kotlin
    implementation(Kotlin.stdlib)

    // AndroidX
    implementation(AndroidX.core)
    implementation(AndroidX.appCompat)
    implementation(AndroidX.lifecycle.runtimeKtx)
    implementation(AndroidX.ui.tooling)
    implementation(AndroidX.fragmentKtx)
    implementation(AndroidX.activityKtx)
    implementation(AndroidX.lifecycle.viewModelKtx)

    // Compose
    implementation(AndroidX.compose.ui)
    implementation(AndroidX.compose.runtime)
    implementation(AndroidX.compose.runtime.liveData)

    // Design
    implementation(Google.android.material)
    implementation(AndroidX.compose.material)
    implementation(AndroidX.compose.material.icons.extended)

    // RxJava2
    implementation(Libs.rxJava2)
    implementation(Libs.rxAndroid)

    // Development
    implementation(JakeWharton.timber)

    testImplementation(Testing.junit)
    androidTestImplementation(AndroidX.test.ext.junitKtx)
    androidTestImplementation(AndroidX.test.espresso.core)
}
