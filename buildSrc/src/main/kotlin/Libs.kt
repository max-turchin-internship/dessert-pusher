object Libs {

    object Versions {
        const val rxJava2Version = "2.2.20"
        const val rxAndroidVersion = "2.1.1"
    }

    const val rxJava2 = "io.reactivex.rxjava2:rxjava:${Versions.rxJava2Version}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroidVersion}"
}
